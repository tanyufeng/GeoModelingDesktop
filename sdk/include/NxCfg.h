/******************************************************************* 
 *  Copyright(c) 2017-2020 Nanjing Normal University
 *  All rights reserved. 
 *   
 *  文件名称: 导出配置
 *  简要描述: 地理数据基类接口文件
 *   
 *  创建日期: 2017-3-2
 *  作者: 张丰源
 *  说明: 用于规定地理数据基本要素的读写
 *   
 ******************************************************************/ 

//Export Dll for WIN64
#ifdef _WIN32
#ifdef _DLL_EXPORT
#define EXPORT_API __declspec(dllexport)
#else
#define EXPORT_API __declspec(dllimport)
#endif
#endif

//Export SO for LINUX
#ifdef _LINUX
#define EXPORT_API __attribute ((visibility("default")))
#endif
