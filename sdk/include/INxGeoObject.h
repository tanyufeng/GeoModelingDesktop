/******************************************************************* 
 *  Copyright(c) 2017-2020 Nanjing Normal University
 *  All rights reserved. 
 *   
 *  文件名称: 地理数据基类接口
 *  简要描述: 地理数据基类接口文件
 *   
 *  创建日期: 2017-3-2
 *  作者: 张丰源
 *  说明: 用于规定地理数据基本要素的读写
 *   
 ******************************************************************/ 

#include "INxUnknown.h"

namespace Nx_Desktop
{
	class INxGeoObject 
	{
	public:
		virtual const char* getId() = 0;

		virtual bool setId(const char * id) = 0;
	};
}
