/******************************************************************* 
 *  Copyright(c) 2017-2020 Nanjing Normal University
 *  All rights reserved. 
 *   
 *  文件名称: 智能指针
 *  简要描述: 用于内存管理，防止内存泄露
 *   
 *  创建日期: 2017-3-3
 *  作者: 张丰源
 *  说明: 智能指针用于方便内存管理，防止内存泄露
 *   
 ******************************************************************/ 

#ifndef __NX_SMART_PTR_H__
#define __NX_SMART_PTR_H__

namespace Nx_Desktop
{
	template <typename T>
	class SmartPtr;

	template <typename T>
	class U_Ptr
	{
	private:
		friend class SmartPtr<T>;

		U_Ptr(T *ptr) :p(ptr), count(1){}

		~U_Ptr(){ delete p; }

		int count;

		T *p;
	};

	template<typename T>
	class SmartPtr
	{
	public:
		SmartPtr(T *ptr):rp(new U_Ptr<T>(ptr)){  }

		SmartPtr(const SmartPtr<T> &sp):rp(sp.rp){ ++rp->count; }

		SmartPtr& operator=(const SmartPtr<T>& rhs) 
		{
			++rhs.rp->count;
			if(--rp->count == 0)
			{
				delete rp;
			}
			rp = rhs.rp;
			return *this;
		}

		T & operator *()
		{
			return *(rp->p);
		}

		T * operator ->()
		{
			return rp->p;
		}

		~SmartPtr()
		{
			if(--rp->count == 0)
			{
				delete rp;
			}
			else
			{
				std::cout << rp->count << " points remained " << std::endl;
			}
		}

	private:
		U_Ptr<T> *rp;
	};
}

}

#endif