/******************************************************************* 
 *  Copyright(c) 2017-2020 Nanjing Normal University
 *  All rights reserved. 
 *   
 *  文件名称: 地理概念图标接口
 *  简要描述: 用于获取地理概念建模的图标的接口
 *   
 *  创建日期: 2017-3-2
 *  作者: 张丰源
 *  说明: 用于获取地理概念建模的图标的接口
 *   
 ******************************************************************/ 

#ifndef __NX_I_GEOICON_H__
#define __NX_I_GEOICON_H__

#include "INxGeoObject.h"

namespace Nx_Desktop
{
	class INxGeoIcon : INxGeoObject
	{
	public:
		virtual const char* getIconId() = 0;

		virtual bool setIconId(const char * iconId) = 0;

		virtual const char* getIconParentId() = 0;

		virtual bool setIconParentId(const char * iconParentId) = 0;

		virtual const char* getIconName() = 0;

		virtual bool setIconName(const char * iconName) = 0;

		virtual const char* getIconFormate() = 0;

		virtual bool setIconFormate(const char * iconFormate) = 0;

	};

}

#endif