/******************************************************************* 
 *  Copyright(c) 2017-2020 Nanjing Normal University
 *  All rights reserved. 
 *   
 *  文件名称: 引用计数基类接口
 *  简要描述: 用于引用计数，方便内存管理，防止内存泄露
 *   
 *  创建日期: 2017-3-3
 *  作者: 张丰源
 *  说明: 引用计数基类接口，所有接口与类的基类，用于引用计数，方便内存管理，防止内存泄露
 *   
 ******************************************************************/ 

#ifndef __NX_I_NX_UNKNOWN_H__
#define __NX_I_NX_UNKNOWN_H__

namespace Nx_Desktop
{
	class INxUnknown
	{
	public:
		INxUnknown():mReferenceCounter(1) { }

		virtual ~INxUnknown(){}

		//! add reference count 
		virtual unsigned int addRef() { return ++mReferenceCounter; };

		//! get the count of reference
		virtual unsigned int getRef() { return mReferenceCounter; }

		//! reduce reference count and release this if count is 0 
		virtual unsigned int relese() 
		{ 
			if(--mReferenceCounter == 0)
			{
				delete this;
				return 0;
			}
			return mReferenceCounter;
		}


	protected:
		//! count of references
		unsigned int mReferenceCounter;
	};
}


#endif